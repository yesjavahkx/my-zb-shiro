drop table if exists category;
create table category
(
    id          varchar(64)                        not null        primary key,
    parent_id   varchar(64)                        null comment '父级类别id',
    name        varchar(255)                       not null comment '类别名称',
    description varchar(500)                       null comment '类别描述',
    creator_id  int                                null comment '创建者id',
    space_id    int      default 1                 null comment '空间Id',
    order_num   int      default 0                 null comment '排序',
    icon        varchar(50)                        null comment '图标',
    status      int(1)   default 1                 null comment '状态：1有效；2删除',
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime default CURRENT_TIMESTAMP null,
    cols        varchar(255)                       null comment '预留字段'
) charset = utf8mb4;

INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1000', '总公司', NULL);
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1001', '北京分公司', '1000');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1002', '上海分公司', '1000');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1003', '北京研发部', '1001');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1004', '北京财务部', '1001');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1005', '北京市场部', '1001');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1006', '北京研发一部', '1003');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1007', '北京研发二部', '1003');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1008', '北京研发一部一小组', '1006');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1009', '北京研发一部二小组', '1006');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1010', '北京研发二部一小组', '1007');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1011', '北京研发二部二小组', '1007');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1012', '北京市场一部', '1005');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1013', '上海研发部', '1002');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1014', '上海研发一部', '1013');
INSERT INTO `category`(`id`, `name`, `parent_id`) VALUES ('1015', '上海研发二部', '1013');

set global log_bin_trust_function_creators=TRUE;


delimiter $$
drop function if exists get_child_list$$
create function get_child_list(in_id varchar(10)) returns varchar(1000)
begin
    declare ids varchar(1000) default '';
    declare tempids varchar(1000);

    set tempids = in_id;
    while tempids is not null do
            set ids = CONCAT_WS(',',ids,tempids);
            select GROUP_CONCAT(id) into tempids from category where FIND_IN_SET(parent_id,tempids)>0;
        end while;
    return ids;
end
$$
delimiter ;
select * from category where find_in_set(id,(get_child_list('1003')));


DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info` (
    `id` varchar(64)   COMMENT '主键id',
    `file_number` int not null auto_increment  COMMENT '文件编号id,从1000 开始自动增长',
    `file_name` varchar(255) not null  COMMENT '文件名字',
    `md5` varchar(32)  COMMENT '文件md5',
    `category_id` varchar(64) COMMENT '文件类别id',
    `category_name` varchar(255) COMMENT '文件类别名称',
    `space_id` varchar(64) COMMENT '空间id',
    `space_name` varchar(255) COMMENT '空间名称',
    `creator_id` int  COMMENT '上传者id',
    `creator_name` varchar(50) COMMENT '上传者名称',
    `file_keys` varchar(255) COMMENT '关键字',
    `detail` text COMMENT '详细说明',
    `contentType` varchar(128)  comment '文件类型',
    `size` int(11)  comment '文件大小',
    `path` varchar(255) COMMENT '物理路径',
    `url`  varchar(1024) ,
    `status` int(1),
    `createTime` datetime ,
    `updateTime` datetime ,
    PRIMARY KEY (`id`),
    key(`file_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `download`;
CREATE TABLE `download` (
    `id` varchar(64)   COMMENT '主键id',
    `download_number` int not null auto_increment  COMMENT '下载的编号,从1000 开始自动增长',
    `applicant_id` int COMMENT '申请人id',
    `application_name` varchar(255)  COMMENT '申请人名字',
    `application_time` datetime COMMENT '申请时间',
    `application_remark` varchar(255)  COMMENT '申请的备注说明',
    `checker_id` int COMMENT '审核人id',
    `checker_name` varchar(255)  COMMENT '审核人名字',
    `check_time` datetime COMMENT '审核时间',
    `check_remark` varchar(255)  COMMENT '审核的备注说明',
    `status` int(1) COMMENT '-1 无需审核，默认通过  0 未审核  1 通过  2 不通过', --
    `zip_password` varchar(255)  COMMENT '解压密码',
    `zip_file` varchar(400)  COMMENT '压缩包文件路径',
    `size` int(11)  comment '文件大小',
    `last_download_time` datetime  comment '最后一次下载时间',
    `download_count` int(11)  comment '下载次数',
    `update_time` datetime ,
    PRIMARY KEY (`id`),
    key(`download_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `download_fileInfo`;
CREATE TABLE `download_fileInfo` (
    `id` varchar(64)   COMMENT '主键id',
    `download_id` varchar(64) COMMENT '下载记录id',
    `fileInfo_id` varchar(50) COMMENT '文件id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
