/*
 Navicat MySQL Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost
 Source Database       : zb-shiro

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : utf-8

 Date: 11/05/2020 06:56:05 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` varchar(64) NOT NULL,
  `parent_id` varchar(64) DEFAULT NULL COMMENT '父级类别id',
  `parent_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL COMMENT '类别名称',
  `description` varchar(500) DEFAULT NULL COMMENT '类别描述',
  `creator_id` int(11) DEFAULT NULL COMMENT '创建者id',
  `space_id` int(11) DEFAULT '1' COMMENT '空间Id',
  `order_num` int(11) DEFAULT '0' COMMENT '排序',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` int(1) DEFAULT '1' COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `cols` varchar(255) DEFAULT NULL COMMENT '预留字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `category`
-- ----------------------------
BEGIN;
INSERT INTO `category` VALUES ('1000', null, '顶层类别', '总公司', '无描述内容', null, '1', '0', null, '1', '2020-08-13 02:38:27', '2020-11-03 00:51:59', null), ('1002', '1000', null, '上海分公司', '无描述内容', null, '1', '0', null, '1', '2020-08-13 02:38:27', '2020-08-13 02:38:27', null), ('1013', '1002', '上海分公司', '上海研发部', '无描述内容', null, '1', '1', null, '1', '2020-08-13 02:38:28', '2020-11-02 20:43:53', null), ('1014', '1013', null, '上海研发一部', '无描述内容', null, '1', '0', null, '1', '2020-08-13 02:38:28', '2020-08-13 02:38:28', null), ('1015', '1013', null, '上海研发二部', '无描述内容', null, '1', '0', null, '1', '2020-08-13 02:38:28', '2020-08-13 02:38:28', null), ('1KVKc9sA', '7hMvn1mV', '成都分公司', '郫都区', '郫都区', null, '1', '2', null, '1', '2020-11-02 19:54:08', '2020-11-05 05:30:11', null), ('27tqYaQW', '1000', '总公司', '123', '', '1', '1', '2', null, '0', '2020-11-03 00:45:17', '2020-11-03 00:45:16', null), ('6OX8fbqj', null, '顶层分类', '分公司2', '', '1', '1', '2', null, '0', '2020-11-03 00:59:42', '2020-11-03 01:04:00', null), ('7hMvn1mV', '1000', '总公司', '成都分公司', '成都', null, '1', '5', null, '1', '2020-08-13 04:16:50', '2020-11-03 00:40:57', null), ('9OlpXd28', '7hMvn1mV', '成都分公司', '春熙路分公司', '春熙路', null, '1', '1', null, '1', '2020-11-01 07:01:25', '2020-11-03 17:09:13', null), ('LCtK9MHB', 'rjD9BNy9', 'admin111', 'ccc', 'ccc', null, '1', '3', null, '0', '2020-11-02 20:20:14', '2020-11-02 20:20:14', null), ('LoCNr0zi', null, '顶层分类', '分公司1', '分公司1', '1', '1', '1', null, '0', '2020-11-03 00:54:29', '2020-11-03 01:05:48', null), ('N0MqzIyB', null, '顶层分类', '', '', null, '1', '0', null, '0', '2020-11-02 20:23:43', '2020-11-02 20:23:43', null), ('oXDKEInd', 'LCtK9MHB', 'ccc', 'aaaA', 'aaaaA', null, '1', '55', null, '0', '2020-11-02 19:53:20', '2020-11-02 19:53:20', null), ('rjD9BNy9', null, '', 'admin111', 'admin111', null, '1', '3', null, '0', '2020-11-02 19:20:05', '2020-11-02 20:48:01', null), ('vJ149d2F', 'rjD9BNy9', 'admin111', 'bbb', 'bbb', null, '1', '2', null, '0', '2020-11-02 20:19:46', '2020-11-02 20:19:46', null), ('ZFlMlIsl', '1000', null, '重庆分公司', '无描述内容', null, '1', '2', null, '1', '2020-08-13 04:29:47', '2020-08-13 04:29:47', null), ('zo5DQtKP', null, '顶层分类', '分公司3', '', '1', '1', '2', null, '0', '2020-11-03 01:00:49', '2020-11-03 01:00:49', null);
COMMIT;

-- ----------------------------
--  Table structure for `download`
-- ----------------------------
DROP TABLE IF EXISTS `download`;
CREATE TABLE `download` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `download_number` int(11) NOT NULL AUTO_INCREMENT COMMENT '下载的编号,从1000 开始自动增长',
  `applicant_id` int(11) DEFAULT NULL COMMENT '申请人id',
  `application_name` varchar(255) DEFAULT NULL COMMENT '申请人名字',
  `application_time` datetime DEFAULT NULL COMMENT '申请时间',
  `application_remark` varchar(255) DEFAULT NULL COMMENT '申请的备注说明',
  `checker_id` int(11) DEFAULT NULL COMMENT '审核人id',
  `checker_name` varchar(255) DEFAULT NULL COMMENT '审核人名字',
  `check_time` datetime DEFAULT NULL COMMENT '审核时间',
  `check_remark` varchar(255) DEFAULT NULL COMMENT '审核的备注说明',
  `status` int(1) DEFAULT NULL COMMENT '-1 无需审核，默认通过  0 未审核  1 通过  2 不通过',
  `zip_password` varchar(255) DEFAULT NULL COMMENT '解压密码',
  `zip_file` varchar(400) DEFAULT NULL COMMENT '压缩包文件路径',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `last_download_time` datetime DEFAULT NULL COMMENT '最后一次下载时间',
  `download_count` int(11) DEFAULT NULL COMMENT '下载次数',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `download_number` (`download_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1033 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `download`
-- ----------------------------
BEGIN;
INSERT INTO `download` VALUES ('1000000035187241', '1030', '2', '陈成成', '2020-11-05 03:02:32', 'ceshi 1', '1', '黄柯翔', '2020-11-05 03:14:05', 'okok', '1', '16b9', null, null, null, null, '2020-11-05 03:14:05'), ('1000000855847118', '1031', '2', '陈成成', '2020-11-05 03:02:50', 'ooo', '1', '黄柯翔', '2020-11-05 03:14:00', 'ok', '1', '65dd', null, null, null, null, '2020-11-05 03:14:00'), ('1000001226499251', '1032', '2', '陈成成', '2020-11-05 05:32:11', 'okok', null, null, null, null, '0', '3a63', null, null, null, null, '2020-11-05 05:32:11'), ('1000001250959992', '1028', '1', '黄柯翔', '2020-11-04 22:17:17', '好玩的', '1', '黄柯翔', '2020-11-05 00:59:54', '好阿阿斯顿sad手打收到撒多手动阀是否水电费大士大夫撒旦阿斯蒂芬士大夫sdaf阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬大所发生的阿斯蒂芬阿斯蒂芬阿斯蒂芬\n发送到发送到发送到发送到发斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬啊手动阀阿斯顿发斯蒂芬阿斯蒂芬发盛世嫡妃浮点数', '2', '1409', '/1028-123的副本父父父(2个文件).zip', null, null, null, '2020-11-05 00:59:54'), ('1000001714762187', '1027', '1', '黄柯翔', '2020-11-03 22:46:53', 'YUDNDF', '1', '黄柯翔', '2020-11-05 05:12:21', 'ok', '1', 'eea5', '/1027-1-1 封面(1) (1)(3个文件).zip', null, null, null, '2020-11-05 05:12:21'), ('1000001931948759', '1029', '1', '黄柯翔', '2020-11-04 22:59:11', '拿来用一下', '1', '黄柯翔', '2020-11-05 00:59:16', 'no', '2', 'ff87', '/1029-计算机网络实验教学管理系统(2个文件).zip', null, null, null, '2020-11-05 00:59:16');
COMMIT;

-- ----------------------------
--  Table structure for `download_fileInfo`
-- ----------------------------
DROP TABLE IF EXISTS `download_fileInfo`;
CREATE TABLE `download_fileInfo` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `download_id` varchar(64) DEFAULT NULL COMMENT '下载记录id',
  `fileInfo_id` varchar(50) DEFAULT NULL COMMENT '文件id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `download_fileInfo`
-- ----------------------------
BEGIN;
INSERT INTO `download_fileInfo` VALUES ('1000000066404746', '1000001931948759', '1000000115454406'), ('1000000192377930', '1000001250959992', '1000001669230793'), ('1000000322515710', '1000000035187241', '1000000115454406'), ('1000000410957772', '1000000035187241', '1000001614630451'), ('1000000430713360', '1000000035187241', '1000001669230793'), ('1000000510042095', '1000001226499251', '1000001370992081'), ('1000000597904347', '1000001250959992', '1000001370992081'), ('1000000615624971', '1000001226499251', '1000000206745211'), ('1000000618285006', '1000001714762187', '1000000206745211'), ('1000000707744564', '1000000035187241', '1000000722305885'), ('1000000797802436', '1000001931948759', '1000001614630451'), ('1000000809782607', '1000000035187241', '1000001862894550'), ('1000000839300474', '1000001226499251', '1000001010529998'), ('1000000974758779', '1000000035187241', '1000000206745211'), ('1000001046851413', '1000001226499251', '1000000115454406'), ('1000001119477345', '1000000035187241', '1000001370992081'), ('1000001222261846', '1000001714762187', '1000000722305885'), ('1000001349138746', '1000001714762187', '1000001862894550'), ('1000001477107507', '1000000855847118', '1000000206745211');
COMMIT;

-- ----------------------------
--  Table structure for `file_info`
-- ----------------------------
DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `file_number` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件编号id,从1000 开始自动增长',
  `file_name` varchar(255) NOT NULL COMMENT '文件名字',
  `md5` varchar(32) DEFAULT NULL COMMENT '文件md5',
  `category_id` varchar(64) DEFAULT NULL COMMENT '文件类别id',
  `category_name` varchar(255) DEFAULT NULL COMMENT '文件类别名称',
  `space_id` varchar(64) DEFAULT NULL COMMENT '空间id',
  `space_name` varchar(255) DEFAULT NULL COMMENT '空间名称',
  `creator_id` int(11) DEFAULT NULL COMMENT '上传者id',
  `creator_name` varchar(50) DEFAULT NULL COMMENT '上传者名称',
  `file_keys` varchar(255) DEFAULT NULL COMMENT '关键字',
  `detail` text COMMENT '详细说明',
  `contentType` varchar(128) DEFAULT NULL COMMENT '文件类型',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `path` varchar(255) DEFAULT NULL COMMENT '物理路径',
  `url` varchar(1024) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `file_number` (`file_number`)
) ENGINE=InnoDB AUTO_INCREMENT=1185 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `file_info`
-- ----------------------------
BEGIN;
INSERT INTO `file_info` VALUES ('1000000115454406', '1183', '计算机网络实验教学管理系统.doc', '952716518d8d496fc9b367aaddfad553', '9OlpXd28', '春熙路分公司', null, null, '1', '黄柯翔', '', '', 'application/wps-writer', '1611776', '/20201104-271651-计算机网络实验教学管理系统.doc', null, '1', '2020-11-04 22:58:14', '2020-11-05 04:37:58'), ('1000000206745211', '1181', '1-1 封面(1) (1).doc', '7ac2ceab3677d7351a57e2a71f701cf4', '1KVKc9sA', '郫都区', null, null, '1', '黄柯翔', '哈哈，你好的 收到', '<p>&nbsp;中华人民共和国万岁</p>', 'application/wps-writer', '151240', '/20201103-c2ceab-1-1 封面(1) (1).doc', null, '1', '2020-11-03 22:46:23', '2020-11-04 00:27:53'), ('1000000722305885', '1180', '1-5+论文正文.doc', '8a9af9b38a00755f7c8494f1472e0a08', '1KVKc9sA', '郫都区', null, null, '1', '黄柯翔', '关键缸盖的', '<h1>5345345345</h1>', 'application/wps-writer', '379011', '/20201105-9af9b3-1-5+论文正文.doc', null, '0', '2020-11-03 22:46:23', '2020-11-05 04:55:50'), ('1000001010529998', '1184', 'bootdemo-0.0.1-SNAPSHOT.jar', '1ee864694e281546bef7dc2ca9c80780', '1KVKc9sA', '郫都区', null, null, '2', '陈成成', null, null, 'application/java-archive', '19942169', '/20201105-e86469-bootdemo-0.0.1-SNAPSHOT.jar', null, '1', '2020-11-05 03:12:49', '2020-11-05 03:12:49'), ('1000001293801293', '1177', '17级实训成绩表-成都上程-区分班级版.xls', '1368d2279d5a9a608d27f1f0d45feed2', '7hMvn1mV', '成都分公司', null, null, '1', '黄柯翔', '关键字1', '说明111', 'application/octet-stream', '98304', '/20201102-68d227-17级实训成绩表-成都上程-区分班级版.xls', null, '0', '2020-11-02 18:46:05', '2020-11-02 18:59:05'), ('1000001370992081', '1176', '2_2020恒安嘉新招聘—乌鲁木齐专场.pdf', '9f2bfb95a7dec281e627f67693e61247', 'ZFlMlIsl', '重庆分公司', null, null, '1', '黄柯翔', '关键字26666666666', '<b><p><b></b></p><p></p><ul><li><b><b><p><b></b></p><p><b>内容1</b></p><p></p></b></b></li><li><b><b><p><b></b></p><p><b>内容2</b></p><p></p></b></b></li></ul><p></p><p></p></b><p></p><p></p>', 'application/pdf', '1813418', '/20201103-2bfb95-2_2020恒安嘉新招聘—乌鲁木齐专场.pdf', null, '1', '2020-11-02 18:46:05', '2020-11-03 22:35:41'), ('1000001614630451', '1182', '2017级《JavaEE程序设计》课程实践成绩单.xls', '49f83b1caae0f90a149e23d0df96ddaf', '9OlpXd28', '春熙路分公司', null, null, '1', '黄柯翔', '好的', '<p>胜多负少的</p>', 'application/octet-stream', '133632', '/20201104-f83b1c-2017级《JavaEE程序设计》课程实践成绩单.xls', null, '0', '2020-11-04 22:58:14', '2020-11-05 04:20:50'), ('1000001669230793', '1178', '123的副本父父父.jpg', '6c52e313bbbdcb20bc0d0a86e0ff7b3a', '9OlpXd28', '333', null, null, '1', '黄柯翔', '关键字3', '<h1>说明333反反复复付付</h1>', 'image/jpeg', '22048', '/20201102-52e313-123的副本.jpg', null, '0', '2020-11-02 18:46:22', '2020-11-05 03:29:46'), ('1000001862894550', '1179', '1-1 封面(2).doc', '7ac2ceab3677d7351a57e2a71f701cf4', '1KVKc9sA', '郫都区', null, null, '1', '黄柯翔', null, null, 'application/wps-writer', '151240', '/20201103-c2ceab-1-1 封面(2).doc', null, '0', '2020-11-03 22:46:23', '2020-11-05 03:21:21');
COMMIT;

-- ----------------------------
--  Table structure for `nodelist`
-- ----------------------------
DROP TABLE IF EXISTS `nodelist`;
CREATE TABLE `nodelist` (
  `id` int(11) DEFAULT NULL,
  `nodecontent` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `nodelist`
-- ----------------------------
BEGIN;
INSERT INTO `nodelist` VALUES ('1', 'a', null), ('2', 'b', '1'), ('3', 'c', '1'), ('4', 'd', '2'), ('5', 'e', '3'), ('6', 'f', '3'), ('7', 'g', '5'), ('8', 'h', '7'), ('9', 'i', '8'), ('10', 'j', '8');
COMMIT;

-- ----------------------------
--  Table structure for `permission`
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` varchar(20) NOT NULL COMMENT '权限id',
  `name` varchar(100) NOT NULL COMMENT '权限名称',
  `description` varchar(255) DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) DEFAULT NULL COMMENT '权限访问路径',
  `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `parent_id` int(11) DEFAULT NULL COMMENT '父级权限id',
  `type` int(1) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `order_num` int(3) DEFAULT '0' COMMENT '排序',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `permission`
-- ----------------------------
BEGIN;
INSERT INTO `permission` VALUES ('1', '1', '工作台', '工作台', '/workdest', 'workdest', '0', '1', '1', 'fa fa-home', '1', '2017-09-27 21:22:02', '2018-02-27 10:53:14'), ('2', '2', '权限管理', '权限管理', null, null, '0', '0', '8', 'fa fa-th-list', '1', '2017-07-13 15:04:42', '2020-10-25 20:50:01'), ('3', '201', '用户列表', '用户管理', '/users', 'users', '36', '1', '1', 'fa fa-circle-o', '1', '2017-07-13 15:05:47', '2020-08-05 02:38:31'), ('4', '20101', '列表查询', '用户列表查询', '/user/list', 'user:list', '3', '2', '0', null, '1', '2017-07-13 15:09:24', '2017-10-09 05:38:29'), ('5', '20102', '新增', '新增用户', '/user/add', 'user:add', '3', '2', '0', null, '1', '2017-07-13 15:06:50', '2018-02-28 17:58:46'), ('6', '20103', '编辑', '编辑用户', '/user/edit', 'user:edit', '3', '2', '0', null, '1', '2017-07-13 15:08:03', '2018-02-27 10:53:14'), ('7', '20104', '删除', '删除用户', '/user/delete', 'user:delete', '3', '2', '0', null, '1', '2017-07-13 15:08:42', '2018-02-27 10:53:14'), ('8', '20105', '批量删除', '批量删除用户', '/user/batch/delete', 'user:batchDelete', '3', '2', '0', '', '1', '2018-07-11 01:53:09', '2018-07-11 01:53:09'), ('9', '20106', '分配角色', '分配角色', '/user/assign/role', 'user:assignRole', '3', '2', '0', null, '1', '2017-07-13 15:09:24', '2017-10-09 05:38:29'), ('10', '202', '角色管理', '角色管理', '/roles', 'roles', '2', '1', '2', 'fa fa-circle-o', '1', '2017-07-17 14:39:09', '2018-02-27 10:53:14'), ('11', '20201', '列表查询', '角色列表查询', '/role/list', 'role:list', '10', '2', '0', null, '1', '2017-10-10 15:31:36', '2018-02-27 10:53:14'), ('12', '20202', '新增', '新增角色', '/role/add', 'role:add', '10', '2', '0', null, '1', '2017-07-17 14:39:46', '2018-02-27 10:53:14'), ('13', '20203', '编辑', '编辑角色', '/role/edit', 'role:edit', '10', '2', '0', null, '1', '2017-07-17 14:40:15', '2018-02-27 10:53:14'), ('14', '20204', '删除', '删除角色', '/role/delete', 'role:delete', '10', '2', '0', null, '1', '2017-07-17 14:40:57', '2018-02-27 10:53:14'), ('15', '20205', '批量删除', '批量删除角色', '/role/batch/delete', 'role:batchDelete', '10', '2', '0', '', '1', '2018-07-10 22:20:43', '2018-07-10 22:20:43'), ('16', '20206', '分配权限', '分配权限', '/role/assign/permission', 'role:assignPerms', '10', '2', '0', null, '1', '2017-09-26 07:33:05', '2018-02-27 10:53:14'), ('17', '203', '菜单管理', '资源管理', '/permissions', 'permissions', '2', '1', '1', 'fa fa-circle-o', '1', '2017-09-26 07:33:51', '2020-08-05 02:41:22'), ('18', '20301', '列表查询', '资源列表', '/permission/list', 'permission:list', '17', '2', '0', null, '1', '2018-07-12 16:25:28', '2018-07-12 16:25:33'), ('19', '20302', '新增', '新增资源', '/permission/add', 'permission:add', '17', '2', '0', null, '1', '2017-09-26 08:06:58', '2018-02-27 10:53:14'), ('20', '20303', '编辑', '编辑资源', '/permission/edit', 'permission:edit', '17', '2', '0', null, '1', '2017-09-27 21:29:04', '2018-02-27 10:53:14'), ('21', '20304', '删除', '删除资源', '/permission/delete', 'permission:delete', '17', '2', '0', null, '1', '2017-09-27 21:29:50', '2018-02-27 10:53:14'), ('22', '3', '运维管理', '运维管理', null, null, '0', '0', '9', 'fa fa-th-list', '1', '2018-07-06 15:19:26', '2020-10-25 20:50:09'), ('23', '301', '数据监控', '数据监控', '/database/monitoring', 'database', '22', '1', '1', 'fa fa-circle-o', '1', '2018-07-06 15:19:55', '2018-09-12 13:14:48'), ('24', '4', '系统工具', '系统工具', '', null, '0', '0', '4', 'fa fa-th-list', '0', '2018-07-06 15:20:38', '2020-08-05 02:42:50'), ('25', '401', '图标工具', '图标工具', '/icons', 'icons', '2', '1', '3', 'fa fa-circle-o', '1', '2018-07-06 15:21:00', '2020-08-05 02:40:51'), ('28', '1000000884924014', '在线用户', '在线用户', '/online/users', 'onlineUsers', '36', '1', '4', 'fa fa-circle-o', '1', '2018-07-18 21:00:38', '2020-08-05 02:39:15'), ('29', '1000000433323073', '在线用户查询', '在线用户查询', '/online/user/list', 'onlineUser:list', '28', '2', '0', null, '1', '2018-07-18 21:01:25', '2018-07-19 12:48:04'), ('30', '1000000903407910', '踢出用户', '踢出用户', '/online/user/kickout', 'onlineUser:kickout', '28', '2', '0', null, '1', '2018-07-18 21:41:54', '2018-07-19 12:48:25'), ('31', '1000000851815490', '批量踢出', '批量踢出', '/online/user/batch/kickout', 'onlineUser:batchKickout', '28', '2', '0', '', '1', '2018-07-19 12:49:30', '2018-07-19 12:49:30'), ('32', '1000000151546917', '我的资源', '文件内容', null, null, '0', '0', '3', 'fa fa-th-list', '1', '2020-08-05 02:19:03', '2020-11-03 00:38:42'), ('33', '1000001773085311', '我的上传', '', '/myuploads', 'myuploads', '32', '1', '1', 'fa fa-circle-o', '1', '2020-08-05 02:20:56', '2020-10-25 20:51:14'), ('34', '1000000957531210', '资源浏览', '浏览所有文件', '/files', 'files', '0', '1', '1', 'fa  fa-files-o', '1', '2020-08-05 02:24:59', '2020-11-03 00:38:03'), ('35', '1000000847789263', '我的下载', '', '/mydownloads', 'mydownloads', '32', '1', '2', 'fa fa-circle-o', '1', '2020-08-05 02:35:51', '2020-08-05 02:36:03'), ('36', '1000001575093565', '用户管理', '用户管理', null, null, '0', '0', '6', 'fa fa-th-list', '1', '2020-08-05 02:37:56', '2020-10-30 05:08:31'), ('37', '1000000731132094', '分类管理', '分类管理', '/categorys', 'categorys', '0', '1', '4', 'fa fa-list', '1', '2020-10-10 16:29:24', '2020-11-01 04:27:29'), ('43', '1000000444719389', '下载审核管理', '下载审核', '', '', '0', '0', '5', 'fa fa-th-list', '0', '2020-10-30 00:44:44', '2020-11-05 06:08:05'), ('44', '1000001373445896', '下载审核', '审核用户的下载申请', '/downloads-uncheck', 'downloads-uncheck', '0', '1', '1', 'fa fa-check-square-o', '1', '2020-10-30 00:45:20', '2020-11-05 06:21:40'), ('45', '1000001622953836', '已通过-下载', '审核通过的下载', '/downloads-passed', 'downloads-passed', '43', '1', '2', 'fa fa-circle-o', '0', '2020-10-30 00:46:34', '2020-11-05 06:07:57'), ('46', '1000000493648876', '已拒绝-下载', '已拒绝的下载申请', '/downloads-refuse', 'downloads-refuse', '43', '1', '3', 'fa fa-circle-o', '0', '2020-10-30 00:47:58', '2020-11-05 06:07:36'), ('47', '1000001015032290', '申请下载', '申请下载', '/download/apply', 'download:applicant', '34', '2', '0', null, '1', '2020-10-31 02:26:45', '2020-11-05 04:15:55'), ('48', '1000001305231446', '直接下载', '直接下载', '/download/immediately', 'download:immediately', '34', '2', '0', null, '1', '2020-10-31 02:29:34', '2020-11-05 04:16:48'), ('49', '1000000920456820', '文件上传', '文件上传', '/fileInfo/submitUpload', 'fileUpload', '34', '2', '0', '', '1', '2020-11-05 03:08:19', '2020-11-05 03:08:19'), ('50', '1000000072387719', '文件编辑', '文件编辑', '/fileInfo/edit_submit', 'fileinfo:edit', '34', '2', '0', null, '1', '2020-11-05 03:26:35', '2020-11-05 03:29:08'), ('51', '1000001188395999', '文件删除', '文件删除', '/fileInfo/delete', 'fileinfo:delete', '34', '2', '0', null, '1', '2020-11-05 03:28:48', '2020-11-05 04:19:29'), ('52', '1000000313034620', '下载', '下载自己上传的文件', '/download/download_my', 'download_my', '33', '2', '0', null, '1', '2020-11-05 04:31:05', '2020-11-05 04:40:18'), ('53', '1000000013268613', '编辑', '编辑我上传的文件', '/fileInfo/edit_my', 'fileinfo:editMy', '33', '2', '0', '', '1', '2020-11-05 04:34:52', '2020-11-05 04:34:52'), ('54', '1000000273801412', '删除', '删除我上传的文件', '/fileInfo/delete-myfileinfo', 'fileinfo:deleteMy', '33', '2', '0', null, '1', '2020-11-05 04:53:26', '2020-11-05 04:54:27'), ('55', '1000000476632703', '下载', '下载审核通过后的文件', '/download/check_yes', 'download:check_yes', '35', '2', '0', '', '1', '2020-11-05 05:01:01', '2020-11-05 05:01:01'), ('56', '1000001144386980', '新增', '新增类别', '/category/add', 'category:add', '37', '2', '0', '', '1', '2020-11-05 05:21:20', '2020-11-05 05:21:20'), ('57', '1000001239157799', '编辑', '编辑分类', '/category/edit', 'category:edit', '37', '2', '0', '', '1', '2020-11-05 05:22:13', '2020-11-05 05:22:13'), ('58', '1000000922683612', '删除', '删除分类', '/category/delete', 'category:delete', '37', '2', '0', null, '1', '2020-11-05 05:23:02', '2020-11-05 05:28:40'), ('59', '1000002052051196', '通过', '审核通过', '/download/check', 'download:check_ok', '44', '2', '0', null, '1', '2020-11-05 05:53:09', '2020-11-05 06:09:27'), ('60', '1000001882751316', '拒绝', '审核拒绝', '/download/check', 'download:check_no', '44', '2', '0', null, '1', '2020-11-05 05:53:39', '2020-11-05 05:54:03'), ('61', '1000001362140602', '已通过的下载', '浏览已通过的下载申请', '/downloads-passed', 'list-downloads-passed', '44', '2', '0', null, '1', '2020-11-05 05:56:18', '2020-11-05 05:57:31'), ('62', '1000001146126330', '已拒绝的下载', '浏览已拒绝的下载申请', '/downloads-refuse', 'list-downloads-refuse', '44', '2', '0', '', '1', '2020-11-05 05:57:21', '2020-11-05 05:57:21');
COMMIT;

-- ----------------------------
--  Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `role`
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES ('1', '1', '超级管理员', '超级管理员', '1', '2017-06-28 20:30:05', '2017-06-28 20:30:10'), ('2', '2', '管理员', '管理员', '1', '2017-06-30 23:35:19', '2017-10-11 09:32:33'), ('3', '3', '普通用户', '普通用户', '1', '2017-06-30 23:35:44', '2018-07-13 11:44:06'), ('4', '1000001092850668', '下载-审核者', '审核普通用户的下载申请', '1', '2020-11-05 03:37:40', null);
COMMIT;

-- ----------------------------
--  Table structure for `role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  `permission_id` varchar(20) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2661 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `role_permission`
-- ----------------------------
BEGIN;
INSERT INTO `role_permission` VALUES ('1609', '1000001092850668', '1000000444719389'), ('1610', '1000001092850668', '1000001373445896'), ('1611', '1000001092850668', '1000001622953836'), ('1612', '1000001092850668', '1000000493648876'), ('2557', '1', '1'), ('2558', '1', '1000000957531210'), ('2559', '1', '1000001015032290'), ('2560', '1', '1000001305231446'), ('2561', '1', '1000000920456820'), ('2562', '1', '1000000072387719'), ('2563', '1', '1000001188395999'), ('2564', '1', '1000001373445896'), ('2565', '1', '1000002052051196'), ('2566', '1', '1000001882751316'), ('2567', '1', '1000001362140602'), ('2568', '1', '1000001146126330'), ('2569', '1', '1000000151546917'), ('2570', '1', '1000001773085311'), ('2571', '1', '1000000313034620'), ('2572', '1', '1000000013268613'), ('2573', '1', '1000000273801412'), ('2574', '1', '1000000847789263'), ('2575', '1', '1000000476632703'), ('2576', '1', '1000000731132094'), ('2577', '1', '1000001144386980'), ('2578', '1', '1000001239157799'), ('2579', '1', '1000000922683612'), ('2580', '1', '1000001575093565'), ('2581', '1', '201'), ('2582', '1', '20101'), ('2583', '1', '20102'), ('2584', '1', '20103'), ('2585', '1', '20104'), ('2586', '1', '20105'), ('2587', '1', '20106'), ('2588', '1', '1000000884924014'), ('2589', '1', '1000000433323073'), ('2590', '1', '1000000903407910'), ('2591', '1', '1000000851815490'), ('2592', '1', '2'), ('2593', '1', '203'), ('2594', '1', '20301'), ('2595', '1', '20302'), ('2596', '1', '20303'), ('2597', '1', '20304'), ('2598', '1', '202'), ('2599', '1', '20201'), ('2600', '1', '20202'), ('2601', '1', '20203'), ('2602', '1', '20204'), ('2603', '1', '20205'), ('2604', '1', '20206'), ('2605', '1', '401'), ('2606', '1', '3'), ('2607', '1', '301'), ('2608', '2', '1'), ('2609', '2', '1000000957531210'), ('2610', '2', '1000001305231446'), ('2611', '2', '1000000920456820'), ('2612', '2', '1000000072387719'), ('2613', '2', '1000001188395999'), ('2614', '2', '1000001373445896'), ('2615', '2', '1000002052051196'), ('2616', '2', '1000001882751316'), ('2617', '2', '1000001362140602'), ('2618', '2', '1000001146126330'), ('2619', '2', '1000000151546917'), ('2620', '2', '1000001773085311'), ('2621', '2', '1000000313034620'), ('2622', '2', '1000000013268613'), ('2623', '2', '1000000273801412'), ('2624', '2', '1000000847789263'), ('2625', '2', '1000000476632703'), ('2626', '2', '1000000731132094'), ('2627', '2', '1000001144386980'), ('2628', '2', '1000001239157799'), ('2629', '2', '1000000922683612'), ('2630', '2', '1000001575093565'), ('2631', '2', '201'), ('2632', '2', '20101'), ('2633', '2', '20102'), ('2634', '2', '20103'), ('2635', '2', '20104'), ('2636', '2', '20105'), ('2637', '2', '20106'), ('2638', '2', '1000000884924014'), ('2639', '2', '1000000433323073'), ('2640', '2', '1000000903407910'), ('2641', '2', '1000000851815490'), ('2642', '2', '2'), ('2643', '2', '202'), ('2644', '2', '20201'), ('2645', '2', '20202'), ('2646', '2', '20203'), ('2647', '2', '20204'), ('2648', '2', '20205'), ('2649', '2', '20206'), ('2650', '3', '1'), ('2651', '3', '1000000957531210'), ('2652', '3', '1000001015032290'), ('2653', '3', '1000000920456820'), ('2654', '3', '1000000151546917'), ('2655', '3', '1000001773085311'), ('2656', '3', '1000000313034620'), ('2657', '3', '1000000013268613'), ('2658', '3', '1000000273801412'), ('2659', '3', '1000000847789263'), ('2660', '3', '1000000476632703');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL,
  `real_name` varchar(100) DEFAULT NULL,
  `salt` varchar(128) DEFAULT NULL COMMENT '加密盐值',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `sex` int(255) DEFAULT NULL COMMENT '年龄：1男2女',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `status` int(1) NOT NULL COMMENT '用户状态：1有效; 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', '1', 'admin', '872359cc44c637cc73b7cd55c06d95e4', '黄柯翔', '8cd50474d2a3c1e88298e91df8bf6f1c', '523179414@qq.com', '187888899991', '1', '22', '1', '2018-05-23 21:22:06', '2020-10-30 04:38:19', '2020-11-05 06:32:16'), ('2', '1000000565444966', 'test', '7859ef826697e829f6aeae5d3e36f1fb', '陈成成', '47a5e2454c07cdb79037672ff0e5be1f', '中华人民共和国中华人民共和国', '13809915022', '1', '23', '1', '2020-08-05 02:10:36', '2020-08-05 02:10:36', '2020-11-05 03:32:14'), ('3', '1000002080333137', '1234', 'a87d45cac62e2c527e887b631f03353c', '一二三四', 'd6d091b6442f512e1d710b37f91a0d01', 'yesjava@126.com', '13809915022', '1', '22', '0', '2020-10-29 23:31:03', '2020-10-29 23:56:54', '2020-10-29 23:31:03'), ('4', '1000000773737617', 'root', 'f2ab3a938a4e91fe73ccf0fb17c97f15', 'superAdmin', '32d7584cd0660aeb3f4ebcc48f1df6ac', 'root@root.com', '12345675434', '1', '99', '1', '2020-11-05 06:26:14', '2020-11-05 06:31:30', '2020-11-05 06:27:58');
COMMIT;

-- ----------------------------
--  Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL COMMENT '用户id',
  `role_id` varchar(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user_role`
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES ('3', '1000000565444966', '3'), ('5', '1000000773737617', '1'), ('6', '1', '2');
COMMIT;

-- ----------------------------
--  Function structure for `get_child_list`
-- ----------------------------
DROP FUNCTION IF EXISTS `get_child_list`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_child_list`(in_id varchar(10)) RETURNS varchar(1000) CHARSET utf8 COLLATE utf8_unicode_ci
begin
    declare ids varchar(1000) default '';
    declare tempids varchar(1000);

    set tempids = in_id;
    while tempids is not null do
            set ids = CONCAT_WS(',',ids,tempids);
            select GROUP_CONCAT(id) into tempids from category where FIND_IN_SET(parent_id,tempids)>0;
        end while;
    return ids;
end
 ;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
